package doni.pdmprova01;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button btn = (Button) findViewById(R.id.the_last_of_us);
        btn.setOnClickListener(new OnClickListenerCustom("The Last Of Us", R.drawable.game1));
        btn = (Button) findViewById(R.id.battlefield$);
        btn.setOnClickListener(new OnClickListenerCustom("Battlefield 4", R.drawable.game4));
        btn = (Button) findViewById(R.id.tomb_raider);
        btn.setOnClickListener(new OnClickListenerCustom("Tomb Raider", R.drawable.game2));
        btn = (Button) findViewById(R.id.gtav);
        btn.setOnClickListener(new OnClickListenerCustom("GTA V", R.drawable.game3));

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.about) {
            Toast.makeText(this, "Donizety Baltokoski - " + Calendar.getInstance().get(Calendar.YEAR),
                    Toast.LENGTH_SHORT).show();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.option_menu, menu);
        return true;
    }

    class OnClickListenerCustom implements View.OnClickListener {
        private String title;
        private int imgId;
        public OnClickListenerCustom(String s, int id) {
            super();
            title = s;
            imgId = id;
        }

        @Override
        public void onClick(View v) {
            Intent i = new Intent(MainActivity.this, ObrigadoActivity.class);
            Bundle params = new Bundle();
            params.putString("title", title);
            params.putInt("winner", imgId);
            i.putExtras(params);
            startActivity(i);
        }
    }
}
