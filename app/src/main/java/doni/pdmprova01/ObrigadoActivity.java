package doni.pdmprova01;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class ObrigadoActivity extends AppCompatActivity {

    private final String winner_text = "Obrigado pelo seu voto! A opção escolhida foi: ";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_obrigado);
        TextView tv = (TextView) findViewById(R.id.winner);
        Bundle params = getIntent().getExtras();
        String title = params.getString("title");
        tv.setText(winner_text + title);
        ImageView iv = (ImageView) findViewById(R.id.img_winner);

        iv.setImageResource(params.getInt("winner"));

    }
}
